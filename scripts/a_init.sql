CREATE DATABASE  IF NOT EXISTS `dys` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `dys`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dys
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `capacitor`
--

DROP TABLE IF EXISTS `capacitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `capacitor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `capacitor` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capacitor`
--

LOCK TABLES `capacitor` WRITE;
/*!40000 ALTER TABLE `capacitor` DISABLE KEYS */;
INSERT INTO `capacitor` VALUES (1,1.4),(2,2.13),(3,6.4),(4,5.6);
/*!40000 ALTER TABLE `capacitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `capacitor_filter`
--

DROP TABLE IF EXISTS `capacitor_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `capacitor_filter` (
  `capacitor_id` int NOT NULL,
  `filter_id` int NOT NULL,
  `nb_capacitor` int NOT NULL,
  PRIMARY KEY (`capacitor_id`,`filter_id`),
  KEY `capacitor_filter_fk_filter_idx` (`filter_id`),
  CONSTRAINT `capacitor_filter_fk_capacitor` FOREIGN KEY (`capacitor_id`) REFERENCES `capacitor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `capacitor_filter_fk_filter` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capacitor_filter`
--

LOCK TABLES `capacitor_filter` WRITE;
/*!40000 ALTER TABLE `capacitor_filter` DISABLE KEYS */;
INSERT INTO `capacitor_filter` VALUES (1,1,1),(2,2,1),(3,2,1),(4,3,1);
/*!40000 ALTER TABLE `capacitor_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coil`
--

DROP TABLE IF EXISTS `coil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `coil` (
  `id` int NOT NULL AUTO_INCREMENT,
  `coil` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coil`
--

LOCK TABLES `coil` WRITE;
/*!40000 ALTER TABLE `coil` DISABLE KEYS */;
INSERT INTO `coil` VALUES (1,0.08),(2,0.2),(3,0.1),(4,2);
/*!40000 ALTER TABLE `coil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coil_filter`
--

DROP TABLE IF EXISTS `coil_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `coil_filter` (
  `coil_id` int NOT NULL,
  `filter_id` int NOT NULL,
  `nb_coil` int NOT NULL,
  PRIMARY KEY (`coil_id`,`filter_id`),
  KEY `coil_filter_fk_filter_idx` (`filter_id`),
  CONSTRAINT `coil_filter_fk_coil` FOREIGN KEY (`coil_id`) REFERENCES `coil` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `coil_filter_fk_filter` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coil_filter`
--

LOCK TABLES `coil_filter` WRITE;
/*!40000 ALTER TABLE `coil_filter` DISABLE KEYS */;
INSERT INTO `coil_filter` VALUES (1,3,1),(2,3,1),(3,2,1),(4,1,1);
/*!40000 ALTER TABLE `coil_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment_tips`
--

DROP TABLE IF EXISTS `equipment_tips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `equipment_tips` (
  `id` int NOT NULL AUTO_INCREMENT,
  `is_amplifier` tinyint NOT NULL,
  `tips` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment_tips`
--

LOCK TABLES `equipment_tips` WRITE;
/*!40000 ALTER TABLE `equipment_tips` DISABLE KEYS */;
INSERT INTO `equipment_tips` VALUES (1,0,'Alors vous avez le choix entre un système actif ou système  passif, tout dépend de votre  budget . Si vous choisissez le système passif alors il faudra prévoir d’un seul amplificateur en prenant garde aux points abordés plus haut (la puissance, le rendement et l\'impédance).Le coût de cet amplificateur peut varier en fonction des options proposés, certains possèdent différentes entrées auxiliaires afin de permettre la connexion d’une source externe (en mini-jack ou CINCH ou encore  USB), d’autres utilisent  des tubes électroniques (des lampes) d\'un coût plus élevé, mais il est tout à fait envisageable d\'acquérir un amplificateur simple mais aux qualités auditives évidentes sans pour autant se ruiner .'),(2,1,'Alors vous pouvez vous tourner vers des enceintes passives qui ne nécessitent qu’une amplification externe. Il faudra bien sûr veiller à choisir des enceintes adaptés à l’amplificateur en terme de puissance (nous parlons de puissance RMS ) et d\'impédance comme expliqué précèdemment.Il sera necessaire de preter attention à la connectique entre l’amplificateur et l’enceinte (vérifier la compatibilité des  connections de part et d’autre).');
/*!40000 ALTER TABLE `equipment_tips` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filter`
--

DROP TABLE IF EXISTS `filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `filter` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order` int DEFAULT NULL,
  `cut_freq` float DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `freq_central` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filter`
--

LOCK TABLES `filter` WRITE;
/*!40000 ALTER TABLE `filter` DISABLE KEYS */;
INSERT INTO `filter` VALUES (1,NULL,NULL,'correction',3000),(2,3,8000,'passe-haut',NULL),(3,3,6100,'passe-bas',NULL);
/*!40000 ALTER TABLE `filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filter_ls`
--

DROP TABLE IF EXISTS `filter_ls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `filter_ls` (
  `ls_id` int NOT NULL,
  `filter_id` int NOT NULL,
  `nb_lane` int NOT NULL,
  PRIMARY KEY (`ls_id`,`filter_id`),
  KEY `filter_ls_fk_filter_idx` (`filter_id`),
  CONSTRAINT `filter_ls_fk_filter` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `filter_ls_fk_ls` FOREIGN KEY (`ls_id`) REFERENCES `loud_speaker` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filter_ls`
--

LOCK TABLES `filter_ls` WRITE;
/*!40000 ALTER TABLE `filter_ls` DISABLE KEYS */;
INSERT INTO `filter_ls` VALUES (1,1,1),(1,3,2),(2,2,2);
/*!40000 ALTER TABLE `filter_ls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `creation_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int NOT NULL,
  `module` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path_UNIQUE` (`path`),
  KEY `history_fk_user_idx` (`user_id`),
  CONSTRAINT `history_fk_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loud_speaker`
--

DROP TABLE IF EXISTS `loud_speaker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loud_speaker` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `diameter` float NOT NULL,
  `freq_res` float NOT NULL,
  `brand` varchar(45) NOT NULL,
  `vas` float DEFAULT NULL,
  `qts` float DEFAULT NULL,
  `xmax` float DEFAULT NULL,
  `max_power` float NOT NULL,
  `nom_power` float NOT NULL,
  `ebp` float DEFAULT NULL,
  `impedance` float NOT NULL,
  `sensitivity` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loud_speaker`
--

LOCK TABLES `loud_speaker` WRITE;
/*!40000 ALTER TABLE `loud_speaker` DISABLE KEYS */;
INSERT INTO `loud_speaker` VALUES (1,'large-bande',21,38,'Visaton BG20',110,0.44,2,70,40,75,8,92),(2,'tweeter',9,4500,'Monacor RBT-95',NULL,NULL,NULL,60,30,NULL,8,98);
/*!40000 ALTER TABLE `loud_speaker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resistor`
--

DROP TABLE IF EXISTS `resistor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resistor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `resistor` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resistor`
--

LOCK TABLES `resistor` WRITE;
/*!40000 ALTER TABLE `resistor` DISABLE KEYS */;
INSERT INTO `resistor` VALUES (1,6.2),(2,6.68);
/*!40000 ALTER TABLE `resistor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resistor_filter`
--

DROP TABLE IF EXISTS `resistor_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resistor_filter` (
  `resistor_id` int NOT NULL,
  `filter_id` int NOT NULL,
  `nb_resistor` int NOT NULL,
  PRIMARY KEY (`resistor_id`,`filter_id`),
  KEY `resistor_filter_fk_filter_idx` (`filter_id`),
  CONSTRAINT `resistor_filter_fk_filter` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `resistor_filter_fk_resistor` FOREIGN KEY (`resistor_id`) REFERENCES `resistor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resistor_filter`
--

LOCK TABLES `resistor_filter` WRITE;
/*!40000 ALTER TABLE `resistor_filter` DISABLE KEYS */;
INSERT INTO `resistor_filter` VALUES (1,2,1),(1,3,1),(2,1,1);
/*!40000 ALTER TABLE `resistor_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_shape`
--

DROP TABLE IF EXISTS `room_shape`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_shape` (
  `id` int NOT NULL AUTO_INCREMENT,
  `shape` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shape_UNIQUE` (`shape`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_shape`
--

LOCK TABLES `room_shape` WRITE;
/*!40000 ALTER TABLE `room_shape` DISABLE KEYS */;
INSERT INTO `room_shape` VALUES (1,'carré'),(2,'rectangle');
/*!40000 ALTER TABLE `room_shape` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_size`
--

DROP TABLE IF EXISTS `room_size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_size` (
  `id` int NOT NULL AUTO_INCREMENT,
  `size` int NOT NULL,
  `is_bigger` tinyint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_size`
--

LOCK TABLES `room_size` WRITE;
/*!40000 ALTER TABLE `room_size` DISABLE KEYS */;
INSERT INTO `room_size` VALUES (3,15,0),(4,15,1);
/*!40000 ALTER TABLE `room_size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_tips`
--

DROP TABLE IF EXISTS `room_tips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_tips` (
  `room_size_id` int NOT NULL,
  `room_shape_id` int NOT NULL,
  `is_absorption` tinyint NOT NULL,
  `tips` mediumtext,
  PRIMARY KEY (`room_size_id`,`room_shape_id`,`is_absorption`),
  KEY `tips_fk_shape_idx` (`room_shape_id`),
  CONSTRAINT `tips_fk_shape` FOREIGN KEY (`room_shape_id`) REFERENCES `room_shape` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `tips_fk_size` FOREIGN KEY (`room_size_id`) REFERENCES `room_size` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_tips`
--

LOCK TABLES `room_tips` WRITE;
/*!40000 ALTER TABLE `room_tips` DISABLE KEYS */;
INSERT INTO `room_tips` VALUES (3,1,0,'Dans ce cas, une enceinte compact d’environ 15 litres sera suffisant.il est préferable de placer vos enceintes en hauteur (il existe des pieds spéciaux ou sur un meuble), idéalement à hauteur de visage lorsque vous êtes assis .'),(3,1,1,'Il est préférable de  choisir une enceinte compacte dite “bibliothèque” présentant l’avantage d’une faible largeur , hauteur et profondeur  donc  la couleur du son s’en trouvera respectée.Pour ce cas de configuration une paire d\'enceintes compactes de maximum 20 Litres est suffisant.'),(3,2,0,'Dans ce cas, une enceinte compacte d’environ 15 litres sera suffisant.L\'idéal est de permettre le placement de vos enceintes en hauteur (il existe des pieds spéciaux ou directement sur un meuble), l’essentiel est de positionner les enceintes à hauteur de visage en position assise  .'),(3,2,1,'Privilégier les enceintes colonnes 2 voies.Ce sont des enceintes plus grandes, les hauts-parleurs sont plus espacés et les fréquences porteront plus loin dans la pièce(le schéma fréquentiel sera mieux respecté), ce qui représentera un avantage pour une pièce rectangulaire et absorbante.'),(4,1,0,'Dans ce cas une enceinte compacte entre 25 et 30 Litres sera appropriée.'),(4,1,1,'Ce cas va nécessiter une enceinte colonne 2 voies.'),(4,2,0,'Dans le cas le une enceinte colonne 2 voies suffit.'),(4,2,1,'Ici, l\'idéal serait l’installation d’une enceinte colonne 3 voies dans le but d’obtenir une bonne répartition de chaque plages de fréquences (aigus, médium et grave).Ainsi on apporte un support médium ce qui va contrebalancer l’effet d’absorption des fréquences plus aiguës.');
/*!40000 ALTER TABLE `room_tips` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `speaker`
--

DROP TABLE IF EXISTS `speaker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `speaker` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `freq_res` float DEFAULT NULL,
  `volume` float NOT NULL,
  `loud_speaker_id` int NOT NULL,
  `vent_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ls_fk_idx` (`loud_speaker_id`),
  KEY `speaker_fk_vent_idx` (`vent_id`),
  CONSTRAINT `speaker_fk_ls` FOREIGN KEY (`loud_speaker_id`) REFERENCES `loud_speaker` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `speaker_fk_vent` FOREIGN KEY (`vent_id`) REFERENCES `vent` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `speaker`
--

LOCK TABLES `speaker` WRITE;
/*!40000 ALTER TABLE `speaker` DISABLE KEYS */;
INSERT INTO `speaker` VALUES (1,'Bass-Reflex',NULL,132,1,1),(2,'tweeter',4500,1,2,NULL);
/*!40000 ALTER TABLE `speaker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name_fr` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name_fr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'Administrateur'),(2,'Membre');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(60) NOT NULL,
  `id_status` int NOT NULL,
  `is_confirmed` tinyint NOT NULL DEFAULT '0',
  `hash` varchar(255) DEFAULT NULL,
  `lost_password_hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `status_fk_idx` (`id_status`),
  CONSTRAINT `status_fk` FOREIGN KEY (`id_status`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Amélie','Courtin','ameliecourtin3@gmail.com','$2b$12$KHFk03bd6ZWaHZcFSj8WmuEGvM.kuUDrxv9piCORGLuwsSlbm7i7W',1,1,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vent`
--

DROP TABLE IF EXISTS `vent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vent` (
  `id` int NOT NULL AUTO_INCREMENT,
  `length` float NOT NULL,
  `width` float NOT NULL,
  `height` float NOT NULL,
  `freq_res` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vent`
--

LOCK TABLES `vent` WRITE;
/*!40000 ALTER TABLE `vent` DISABLE KEYS */;
INSERT INTO `vent` VALUES (1,0.3,0.25,0.1,34);
/*!40000 ALTER TABLE `vent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'dys'
--
/*!50003 DROP PROCEDURE IF EXISTS `capacitor_getByFilterId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `capacitor_getByFilterId`(filter_id INT)
BEGIN
	SELECT 
		cf.nb_capacitor, c.capacitor
	FROM
		capacitor_filter cf
			JOIN
		capacitor c ON c.id = cf.capacitor_id
	WHERE
		cf.filter_id = filter_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `coil_getByFilterId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `coil_getByFilterId`(filter_id INT)
BEGIN
	SELECT 
		cf.nb_coil, c.coil
	FROM
		coil_filter cf
			JOIN
		coil c ON c.id = cf.coil_id
	WHERE
		cf.filter_id = filter_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filter_GetByLsIdAndNbLane` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filter_GetByLsIdAndNbLane`(ls_id INT, nb_lane INT)
BEGIN
	SELECT 
    f.id, f.order, f.cut_freq, f.type, f.freq_central
FROM
    dys.filter_ls fl
        JOIN
    filter f ON f.id = fl.filter_id
WHERE
    fl.ls_id = ls_id AND fl.nb_lane = nb_lane;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_equipment_tip` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_equipment_tip`(has_amplifier TINYINT)
BEGIN
	SELECT
		et.tips
    FROM
		equipment_tips et
	WHERE
		et.is_amplifier = has_amplifier;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_room_tip` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_room_tip`(rs_id INT, rsi_id INT, is_absorption TINYINT)
BEGIN
	SELECT 
    rt.tips
	FROM
		room_tips rt
			JOIN
		room_shape rs ON rs.id = rt.room_shape_id AND rs.id = rs_id
			JOIN
		room_size rsi ON rsi.id = rt.room_size_id AND rsi.id = rsi_id
	WHERE
		rt.is_absorption = is_absorption;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_speaker_ls_vent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_speaker_ls_vent`(speaker_types VARCHAR(255))
BEGIN
	SELECT
    s.id 's_id',
    s.type 's_type',
    s.volume 's_volume',
    ls.id 'ls_id',
    ls.type 'ls_type',
    ls.diameter 'ls_diameter',
    ls.freq_res 'ls_freq_res',
    ls.brand 'ls_brand',
    ls.vas 'ls_vas',
    ls.qts 'ls_qts',
    ls.xmax 'ls_xmax',
    ls.max_power 'ls_max_power',
    ls.ebp 'ls_ebp',
    ls.impedance 'ls_impedance',
    ls.sensitivity 'ls_sensitivity',
    v.id 'v_id',
    v.length 'v_length',
    v.width 'v_width',
    v.height 'v_height',
    v.freq_res 'v_freq_res'
FROM
    speaker s
        JOIN
    loud_speaker ls ON s.loud_speaker_id = ls.id
        LEFT JOIN
    vent v ON s.vent_id = v.id
WHERE
    find_in_set(s.type, speaker_types);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `history_getFromId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `history_getFromId`(id INT)
BEGIN
	SELECT
		h.path,
        h.user_id
    FROM 
		history h
	WHERE
		h.id = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `history_getFromUserId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `history_getFromUserId`(user_id INT)
BEGIN
	SELECT
		*
	FROM
		history h
	WHERE
		h.user_id = user_id
	ORDER BY
		h.creation_date DESC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `history_getIdFromPath` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `history_getIdFromPath`(path VARCHAR(255))
BEGIN
	SELECT
		h.id
    FROM
		history h
	WHERE
		h.path = path;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `history_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `history_insert`(path VARCHAR(255), user_id INT, module VARCHAR(45))
BEGIN
	INSERT into history(path, user_id, module) VALUES(
		path, user_id, module
	);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `resistor_getByFilterId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `resistor_getByFilterId`(filter_id INT)
BEGIN
	SELECT 
		rf.nb_resistor, r.resistor
	FROM
		resistor_filter rf
			JOIN
		resistor r ON r.id = rf.resistor_id
	WHERE
		rf.filter_id = filter_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `room_shape_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `room_shape_get`()
BEGIN
	SELECT
		rs.id,
        rs.shape
    FROM
		room_shape rs;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `room_size_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `room_size_get`()
BEGIN
	SELECT
		rs.id,
        rs.size,
        rs.is_bigger
	FROM
		room_size rs;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `status_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `status_get`()
BEGIN
	SELECT
		*
	FROM
		status;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_confirm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_confirm`(user_id INT)
BEGIN
	UPDATE `dys`.`user` SET `is_confirmed` = '1' WHERE (`id` = user_id);
    UPDATE `dys`.`user` SET `hash` = null WHERE (`id` = user_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_Create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_Create`(first_name VARCHAR(255), last_name VARCHAR(255), email VARCHAR(255), password CHAR(60), id_status INT, hash VARCHAR(255))
BEGIN
    INSERT INTO user(first_name, last_name, email, password, id_status, hash) values(
		first_name,
		last_name,
		email,
		password,
		id_status,
        hash
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_create_admin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_create_admin`(first_name VARCHAR(255), last_name VARCHAR(255), email VARCHAR(255), password CHAR(60), id_status INT)
BEGIN
	INSERT into user(first_name, last_name, email, password, id_status, is_confirmed) values(
		first_name, last_name, email, password, id_status, 1
    );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_delete`(user_id INT)
BEGIN
	DELETE FROM `dys`.`user` WHERE (`id` = user_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_GetByEmail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_GetByEmail`(email VARCHAR(255))
BEGIN
	SELECT *
    FROM user u
    WHERE u.email = email;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_getByHash` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_getByHash`(hash VARCHAR(255))
BEGIN
	SELECT
		u.id
	FROM
		user u
	WHERE
		u.hash = hash;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_GetById` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_GetById`(id INT)
BEGIN
	SELECT *
    FROM user
    WHERE user.id = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_getByLostHash` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_getByLostHash`(hash VARCHAR(255))
BEGIN
	SELECT
		*
	FROM
		user 
	WHERE
		lost_password_hash = hash;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_get_admin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_get_admin`()
BEGIN
	SELECT
		u.id,
        u.first_name,
        u.last_name,
        u.email,
        u.id_status,
        s.name_fr AS 'status',
        u.is_confirmed
	FROM
		user u
			JOIN
		status s ON u.id_status = s.id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_setLostHash` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_setLostHash`(user_id INT, lost_hash VARCHAR(255))
BEGIN
	UPDATE `dys`.`user` SET `lost_password_hash` = lost_hash WHERE (`id` = user_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_update_admin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_update_admin`(user_id INT, first_name VARCHAR(255), last_name VARCHAR(255), email VARCHAR(255), status_id INT, is_confirmed TINYINT)
BEGIN
	UPDATE 
		`dys`.`user` 
	SET 
		`first_name` = first_name,
        `last_name` = last_name,
        `email` = email,
        `id_status` = status_id,
        `is_confirmed` = is_confirmed
	WHERE (`id` = user_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_update_email` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_update_email`(user_id INT, email VARCHAR(255))
BEGIN
	UPDATE `dys`.`user` SET `email` = email WHERE (`id` = user_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_update_password` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_update_password`(user_id INT, password CHAR(60))
BEGIN
	UPDATE `dys`.`user` SET `password` = password WHERE (`id` = user_id);
    UPDATE `dys`.`user` SET `lost_password_hash` = null WHERE (`id` = user_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-14 18:36:48
